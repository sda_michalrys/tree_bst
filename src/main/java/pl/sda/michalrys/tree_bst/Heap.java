package pl.sda.michalrys.tree_bst;

import java.util.Arrays;

public class Heap {

    HeapElement[] heapElements = new HeapElement[1000];
    int size;

    class HeapElement {
        int value;

        public HeapElement(int value) {
            this.value = value;
        }

        public int get() {
            return value;
        }

        public void set(int value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value + "";
        }
    }

    public Heap() {
        size = 0;
    }

    public void add(int value) {
        HeapElement newElement = new HeapElement(value);

        // CASE 1 : heap is empty
        if (heapElements[0] == null) {
            heapElements[0] = newElement;
            size++;
            return;
        }

        // CASE 2: heap is not empty
        // put new element in first empty slot
        int idNewElement = size;
        heapElements[idNewElement] = newElement;
        size++;

        // check if new value is > parent
        HeapElement elementTemp;
        HeapElement elementParent;
        int idParent;
        while (true) {
            // get parent from new element
            idParent = (idNewElement - 1) / 2;
            elementParent = heapElements[idParent];

            // check if parent must be replaced by new element
            if (elementParent.get() > heapElements[idNewElement].get()) {
                elementTemp = heapElements[idParent];
                heapElements[idParent] = heapElements[idNewElement];
                heapElements[idNewElement] = elementTemp;
                idNewElement = idParent;
            } else {
                return;
            }
        }

    }



    @Override
    public String toString() {
        String result = "";
        result = Arrays.toString(heapElements);
        return result;
    }

    public static void main(String[] args) {
        System.out.println("HEAP TEST\n");

        Heap heap = new Heap();
        heap.add(10);
        System.out.println(heap);
        heap.add(8);
        System.out.println(heap);
        heap.add(6);
        System.out.println(heap);
        heap.add(9);
        System.out.println(heap);
        heap.add(5);

        System.out.println(heap);
        heap.add(20);
        System.out.println(heap);

    }
}