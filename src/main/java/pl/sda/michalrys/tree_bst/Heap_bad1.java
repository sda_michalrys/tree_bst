package pl.sda.michalrys.tree_bst;

public class Heap_bad1 {

    HeapElement[] values = new HeapElement[1000];

    class HeapElement {
        int value;

        public HeapElement(int value) {
            this.value = value;
        }
    }

    public Heap_bad1() {
    }

    public void add(int value) {
        HeapElement element = new HeapElement(value);

        if (values[0] == null) {
            values[0] = element;
            return;
        }
        int i = 0;
        while (true) {
            if (values[i].value < element.value) {

                int lChild = i * 2 + 1;
                int rChild = (i + 1) * 2;

                if (values[lChild] == null) {
                    values[lChild] = element;
                }
                if (values[rChild] == null) {
                    values[rChild] = element;
                }
                if(values[lChild].value < values[rChild].value) {
                    i = lChild;
                } else {
                    i = rChild;
                }
            } else {

            }


        }


    }
}
