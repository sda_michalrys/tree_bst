package pl.sda.michalrys.tree_bst;

public class TreeBST {
    private Element element;

    private class Element {
        private int payload;
        private Element leftChildren;
        private Element rightChildren;

        public Element(int payload) {
            this.payload = payload;
            leftChildren = null;
            rightChildren = null;
        }

        public int get() {
            return payload;
        }

        public void set(int payload) {
            this.payload = payload;
        }

        public Element getLeftChildren() {
            return leftChildren;
        }

        public void setLeftChildren(Element leftChildren) {
            this.leftChildren = leftChildren;
        }

        public Element getRightChildren() {
            return rightChildren;
        }

        public void setRightChildren(Element rightChildren) {
            this.rightChildren = rightChildren;
        }

        @Override
        public String toString() {
            return payload + "";
        }
    }

    public void add(int payload) {
        Element newElementToAdd = new Element(payload);

        // CASE 1: if queue is empty
        if (this.element == null) {
            this.element = newElementToAdd;
            return;
        }

        // CASE 2: check where to put -> left or right

        Element currentElement = this.element;

        while (true) {
            // CASE 2.1: if new payload is < payload of currentElement -> go left
            if (newElementToAdd.get() < currentElement.get()) {

                // CASE 2.1.1: if left children exits (not null) -> set it as currentElement
                if (currentElement.leftChildren != null) {
                    currentElement = currentElement.leftChildren;
                    continue;
                }

                // CASE 2.1.2: here currentElement.leftChildren = null => STOP. set leftChildren = newElementToAdd. END
                currentElement.setLeftChildren(newElementToAdd);
                return;
            }

            // CASE 2.2: if new payload is > payload of currentElement -> go right
            if (newElementToAdd.get() > currentElement.get()) {

                // CASE 2.2.1: if right children exits (not null) -> set it as currentElement
                if (currentElement.rightChildren != null) {
                    currentElement = currentElement.rightChildren;
                    continue;
                }

                // CASE 2.2.2: here currentElement.rightChildren = null => STOP. set rightChildren = newElementToAdd. END
                currentElement.setRightChildren(newElementToAdd);
                return;
            }

            // CASE 3 - duplicate = exit
            if (newElementToAdd.get() == currentElement.get()) {
                return;
            }
        }
    }

    public boolean contains(int payload) {
        // CASE 1: if queue is empty
        if (this.element == null) {
            return false;
        }

        // CASE 2:
        Element currentElement = this.element;

        while (true) {
            // CASE 2.0: if payload is = payload of currentElement -> TRUE
            if (payload == currentElement.get()) {
                return true;
            }

            // CASE 2.1: if payload is < payload of currentElement -> go left
            if (payload < currentElement.get()) {

                // CASE 2.1.1: if left children exits (not null) -> set it as currentElement
                if (currentElement.leftChildren != null) {
                    currentElement = currentElement.leftChildren;
                    continue;
                }

                // CASE 2.1.2: here currentElement.leftChildren = null => STOP. set leftChildren = newElementToAdd. END
                return false;
            }

            // CASE 2.2: if new payload is > payload of currentElement -> go right
            if (payload > currentElement.get()) {

                // CASE 2.2.1: if right children exits (not null) -> set it as currentElement
                if (currentElement.rightChildren != null) {
                    currentElement = currentElement.rightChildren;
                    continue;
                }

                // CASE 2.2.2: here currentElement.rightChildren = null => STOP. set rightChildren = newElementToAdd. END
                return false;
            }
        }
    }

    public Element getElement() {
        return element;
    }

    public Element getElementLeft() {
        return element.getLeftChildren();
    }

    public Element getElementRight() {
        return element.getRightChildren();
    }

    @Override
    public String toString() {
        String result = "";

        return result;
    }
}
