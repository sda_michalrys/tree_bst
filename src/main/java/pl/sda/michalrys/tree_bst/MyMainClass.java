package pl.sda.michalrys.tree_bst;

import javax.xml.bind.Element;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MyMainClass {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 10_000_00; i++) {
            int number = random.nextInt(100_000_000);
            if (!list.contains(number)) {
                list.add(number);
            }
        }

        // search value
        System.out.println(linearSearch(list, 5));

        System.out.println(treeSearch(list, 5));
    }

    public static boolean linearSearch(List<Integer> list, int target) {
        for (int i : list) {
            if (i == target) {
                return true;
            }
        }
        return false;
    }

    public static boolean treeSearch(List<Integer> list, int target) {
        TreeBST myTree = new TreeBST();

        for (int value : list) {
            myTree.add(value);
        }
        return myTree.contains(target);
    }
}