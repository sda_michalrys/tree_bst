package pl.sda.michalrys.tree_bst;

public class Main {

    public static void main(String[] args) {
        TreeBST tree = new TreeBST();

        tree.add(100);
        tree.add(50);
        tree.add(30);

        System.out.println(tree.getElement());
        System.out.println(tree.getElementLeft());
        System.out.println(tree.getElementRight());

        System.out.println(tree.contains(30));
        System.out.println(tree.contains(50));
        System.out.println(tree.contains(100));
        System.out.println(tree.contains(10));


    }

}
